package com.bsv.bsv_trader.trader_emulator.managers;

import com.bsv.bsv_trader.trader_emulator.models.Account;
import com.bsv.bsv_trader.trader_emulator.models.User;
import com.bsv.bsv_trader.trader_emulator.repo.AccountRepository;
import com.bsv.bsv_trader.trader_emulator.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AccountManager {

    @Autowired
    private AccountRepository accountRepository;

    public Account getAccountById(Long id) {
        return accountRepository.findById(id).orElse(null);
    }
}
