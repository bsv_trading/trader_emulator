package com.bsv.bsv_trader.trader_emulator.managers;

import com.bsv.bsv_trader.trader_emulator.models.User;
import com.bsv.bsv_trader.trader_emulator.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserManager {

    @Autowired
    private UserRepository userRepository;

    public User getUserByName(String name) {
        return userRepository.findByUsername(name);
    }
}
