package com.bsv.bsv_trader.trader_emulator.managers;

import com.bsv.bsv_trader.trader_emulator.models.TradingCurrency;
import com.bsv.bsv_trader.trader_emulator.repo.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CurrencyManager {

    @Autowired
    private CurrencyRepository currencyRepository;

    public TradingCurrency getTradingCurrencyByCode(String code) {
        return Optional.ofNullable(currencyRepository.findByCodeIs(code))
                .orElseGet(Collections::emptyList)
                .stream()
                .findFirst()
                .orElse(null);
    }
}
