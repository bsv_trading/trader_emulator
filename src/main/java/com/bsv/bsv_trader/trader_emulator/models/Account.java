package com.bsv.bsv_trader.trader_emulator.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Accessors(chain = true)
@Getter @Setter
@NoArgsConstructor
public class Account {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;
    protected String description;
    private String baseCurrency;
    private BigDecimal balance;
    @OneToMany
    private List<CurrencyAmount> investmentPortfolio;
    @OneToMany
    private List<StockTransaction> stockTransactions;

    public void addToInvestmentPortfolio(CurrencyAmount currencyAmount) {
        if (investmentPortfolio == null) {
            investmentPortfolio = new ArrayList<>();
        }
        investmentPortfolio.add(currencyAmount);
    }

    public void addToStockTransaction(StockTransaction stockTransaction) {
        if (stockTransactions == null) {
            stockTransactions = new ArrayList<>();
        }
        stockTransactions.add(stockTransaction);
    }

    public Account clone() throws CloneNotSupportedException
    {
        return (Account) super.clone();
    }
}
