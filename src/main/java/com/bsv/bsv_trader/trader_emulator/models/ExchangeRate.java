package com.bsv.bsv_trader.trader_emulator.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Entity
@Accessors(chain = true)
@Getter @Setter
@NoArgsConstructor
public class ExchangeRate {
    @Id
    @GeneratedValue
    protected Long id;
    private LocalDateTime time;
    private Long rate;
}
