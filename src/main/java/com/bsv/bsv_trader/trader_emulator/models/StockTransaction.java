package com.bsv.bsv_trader.trader_emulator.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Entity
@Accessors(chain = true)
@Getter @Setter
@NoArgsConstructor
public class StockTransaction {
    @Id
    @GeneratedValue
    protected Long id;
    private String baseCurrency;
    private String purchasedCurrency;
    private Long amount;
    private Status status;
    private LocalDateTime openDateTime;
    private LocalDateTime lastChangesDateTime;
}
