package com.bsv.bsv_trader.trader_emulator.models;

public enum Status {
    OPEN,
    PROCESSING,
    CLOSED,
    REJECTED
}
