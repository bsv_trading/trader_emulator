package com.bsv.bsv_trader.trader_emulator.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Accessors(chain = true)
@Getter @Setter
@NoArgsConstructor
public class CurrencyAmount {
    @Id
    @GeneratedValue
    protected Long id;
    private String currencyCode;
    private Long amount;
}
