package com.bsv.bsv_trader.trader_emulator.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Entity
@Accessors(chain = true)
@Getter @Setter
@NoArgsConstructor
public class TradingCurrency {
    @Id
    @GeneratedValue
    protected Long id;
    protected String code;
    protected String description;
    private String baseCurrency;
    private String tradingCurrency;
    private Long currentExchangeRate;

    @OneToMany
    private List<ExchangeRate> exchangeRatesHistory;

    public void addExchangeRatesHistory(ExchangeRate exchangeRate) {
        if (exchangeRatesHistory == null) {
            exchangeRatesHistory = new ArrayList<>();
        }
        exchangeRatesHistory.add(exchangeRate);
    }

    public TradingCurrency clone() throws CloneNotSupportedException
    {
        return (TradingCurrency) super.clone();
    }
}
