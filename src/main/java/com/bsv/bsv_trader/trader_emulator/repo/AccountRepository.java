package com.bsv.bsv_trader.trader_emulator.repo;

import com.bsv.bsv_trader.trader_emulator.models.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
}
