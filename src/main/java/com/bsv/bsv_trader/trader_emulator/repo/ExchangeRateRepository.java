package com.bsv.bsv_trader.trader_emulator.repo;

import com.bsv.bsv_trader.trader_emulator.models.ExchangeRate;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ExchangeRateRepository extends CrudRepository<ExchangeRate, Long> {
    List<ExchangeRate> findAll();
}
