package com.bsv.bsv_trader.trader_emulator.repo;

import com.bsv.bsv_trader.trader_emulator.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
