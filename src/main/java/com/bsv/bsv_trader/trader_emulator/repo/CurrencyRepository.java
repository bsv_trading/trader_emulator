package com.bsv.bsv_trader.trader_emulator.repo;

import com.bsv.bsv_trader.trader_emulator.models.TradingCurrency;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CurrencyRepository extends CrudRepository<TradingCurrency, Long> {
    List<TradingCurrency> findAll();
    List<TradingCurrency> findByCodeIs(String code);
}
