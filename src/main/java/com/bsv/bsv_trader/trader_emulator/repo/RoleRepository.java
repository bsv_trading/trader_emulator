package com.bsv.bsv_trader.trader_emulator.repo;

import com.bsv.bsv_trader.trader_emulator.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
