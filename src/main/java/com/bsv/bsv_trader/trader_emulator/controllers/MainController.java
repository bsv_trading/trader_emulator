package com.bsv.bsv_trader.trader_emulator.controllers;

import com.bsv.bsv_trader.trader_emulator.models.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;

@Controller
public class MainController {

    @GetMapping("/")
    public String home(Model model, Authentication authentication) {
        model.addAttribute("title", "Home page");

        User user = (User) Optional.ofNullable(authentication)
                .map(Authentication::getPrincipal)
                .orElse(null);
        if (user != null) {
            model.addAttribute("username", user.getUsername());
        }
        return "home";
    }
}
