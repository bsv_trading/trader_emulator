package com.bsv.bsv_trader.trader_emulator.controllers;

import com.bsv.bsv_trader.trader_emulator.managers.AccountManager;
import com.bsv.bsv_trader.trader_emulator.models.Account;
import com.bsv.bsv_trader.trader_emulator.models.TradingCurrency;
import com.bsv.bsv_trader.trader_emulator.models.User;
import com.bsv.bsv_trader.trader_emulator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping({"/account"})
public class AccountController {

    @Autowired
    private AccountManager accountManager;

    @GetMapping("/info")
    public String getUserInfo(Model model, Authentication authentication, @RequestParam Long id) {

        Account account = accountManager.getAccountById(id);

        model.addAttribute("name", account.getName());
        model.addAttribute("description", account.getDescription());
        model.addAttribute("baseCurrency", account.getBaseCurrency());
        model.addAttribute("balance", account.getBalance());
        model.addAttribute("investmentPortfolio", account.getInvestmentPortfolio());
        model.addAttribute("stockTransactions", account.getStockTransactions());

        var investmentPortfolioGraphic = Optional.ofNullable(account.getInvestmentPortfolio())
                .orElseGet(Collections::emptyList)
                .stream()
                .map(exchangeRate -> List.of(exchangeRate.getCurrencyCode(), exchangeRate.getAmount()))
                .toList();
        model.addAttribute("chartData", investmentPortfolioGraphic);
        return "account_info";
    }

    @PostMapping("/update")
    public String updateUser(Model model, Authentication authentication) {
        return "redirect:/";
    }

//    private User getUser(Authentication authentication) {
//        return userService.findByUserName(getUserName(authentication));
//    }
//
//    private String getUserName(Authentication authentication) {
//        return Optional.ofNullable(authentication)
//                .map(Authentication::getPrincipal)
//                .map(User.class::cast)
//                .map(User::getUsername)
//                .orElse(null);
//    }
}
