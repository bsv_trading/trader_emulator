package com.bsv.bsv_trader.trader_emulator.controllers;

import com.bsv.bsv_trader.trader_emulator.models.Role;
import com.bsv.bsv_trader.trader_emulator.models.User;
import com.bsv.bsv_trader.trader_emulator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping({"/user"})
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/info")
    public String getUserInfo(Model model, Authentication authentication) {
        User user = getUser(authentication);
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        model.addAttribute("roles", getUserRoles(user));
        model.addAttribute("accounts", user.getAccounts());
        return "user_info";
    }

    @PostMapping("/update")
    public String updateUser(Model model, Authentication authentication) {
        return "redirect:/";
    }

    private User getUser(Authentication authentication) {
        return userService.findByUserName(getUserName(authentication));
    }

    private String getUserName(Authentication authentication) {
        return Optional.ofNullable(authentication)
                .map(Authentication::getPrincipal)
                .map(User.class::cast)
                .map(User::getUsername)
                .orElse(null);
    }

    private String getUserRoles(User user) {
        return Optional.ofNullable(user)
                .map(User::getRoles)
                .orElseGet(Collections::emptySet)
                .stream()
                .map(Role::getName)
                .collect(Collectors.joining(", "));
    }
}
