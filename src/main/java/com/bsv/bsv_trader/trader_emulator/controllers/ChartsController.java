package com.bsv.bsv_trader.trader_emulator.controllers;

import com.bsv.bsv_trader.trader_emulator.managers.CurrencyManager;
import com.bsv.bsv_trader.trader_emulator.models.TradingCurrency;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Controller
@RequiredArgsConstructor
public class ChartsController {
    private static final Random RANDOM = new Random(System.currentTimeMillis());

    @Autowired
    private final CurrencyManager currencyManager;

    @GetMapping("/chart/pie")
    public String pieChart(Model model) {
        model.addAttribute("chartData", getChartData());
        return "charts/pie_chart";
    }

    @GetMapping("/chart/line")
    public String lineChart() {
        return "charts/line_chart";
    }

    @GetMapping("/chart/exchange_rate_history")
    public String exchangeRateHistory(Model model) {

        var exchangeRatesHistory = Optional.ofNullable(currencyManager.getTradingCurrencyByCode("USD/RUB"))
                .map(TradingCurrency::getExchangeRatesHistory)
                .orElseGet(Collections::emptyList)
                .stream()
                .map(exchangeRate -> List.of(exchangeRate.getTime().toString(), exchangeRate.getRate()))
                .toList();


        model.addAttribute("chartData", exchangeRatesHistory);
        return "charts/exchange_rate_history";
    }

    private List<List<Object>> getChartData() {
        return List.of(
                List.of("Mushrooms", RANDOM.nextInt(5)),
                List.of("Onions", RANDOM.nextInt(5)),
                List.of("Olives", RANDOM.nextInt(5)),
                List.of("Zucchini", RANDOM.nextInt(5)),
                List.of("Pepperoni", RANDOM.nextInt(5))
        );
    }
}
