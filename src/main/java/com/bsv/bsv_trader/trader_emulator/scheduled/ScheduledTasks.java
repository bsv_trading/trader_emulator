package com.bsv.bsv_trader.trader_emulator.scheduled;

import com.bsv.bsv_trader.trader_emulator.models.TradingCurrency;
import com.bsv.bsv_trader.trader_emulator.models.ExchangeRate;
import com.bsv.bsv_trader.trader_emulator.repo.CurrencyRepository;
import com.bsv.bsv_trader.trader_emulator.repo.ExchangeRateRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@Component
public class ScheduledTasks {
    private static final Random RANDOM = new Random(System.currentTimeMillis());

    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    private ExchangeRateRepository exchangeRateRepository;

//    @Transactional
//    @Scheduled(fixedDelay = 20000)
//    public void emulationUpdatingExchangeRate() {
//        List<TradingCurrency> currenciesUsdRub = currencyRepository.findByCodeIs("USD/RUB");
//        TradingCurrency updatedTradingCurrency = createOrUpdateTradingCurrency (currenciesUsdRub);
//        ExchangeRate newExchangeRate = updatedTradingCurrency.getExchangeRatesHistory().get(updatedTradingCurrency.getExchangeRatesHistory().size() - 1);
//        exchangeRateRepository.save(newExchangeRate);
//        currencyRepository.save(updatedTradingCurrency);
//    }

    private TradingCurrency createOrUpdateTradingCurrency (List<TradingCurrency> currencies) {
        TradingCurrency tradingCurrency;
        if (currencies.isEmpty()) {
            tradingCurrency = new TradingCurrency()
                .setCode("USD/RUB")
                .setBaseCurrency("USD")
                .setTradingCurrency("RUB")
                .setDescription("Currency trading instrument")
                .setCurrentExchangeRate(90L);
        } else {
            tradingCurrency = currencies.get(0);
        }
        updateExchangeRate(tradingCurrency);
        return tradingCurrency;
    }

    private void updateExchangeRate(TradingCurrency tradingCurrency) {
        if(tradingCurrency.getCurrentExchangeRate() == null) {
            tradingCurrency.setCurrentExchangeRate(90L);
            tradingCurrency.addExchangeRatesHistory(
                new ExchangeRate()
                    .setTime(LocalDateTime.now())
                    .setRate(90L)
            );
            return;
        }
        tradingCurrency.addExchangeRatesHistory(
                new ExchangeRate()
                        .setTime(LocalDateTime.now())
                        .setRate(tradingCurrency.getCurrentExchangeRate())
        );

        tradingCurrency.setCurrentExchangeRate(tradingCurrency.getCurrentExchangeRate() + RANDOM.nextLong(3) - 1L);
    }
}
